//
//  ViewController.swift
//  Timer
//
//  Created by Olzhas Akhmetov on 19.03.2021.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func setTimer(_ sender: Any) {
        let time = Int(textField.text!) ?? 0
        
        let secondVC = storyboard?.instantiateViewController(withIdentifier: "secondVC") as! SecondViewController
        
        secondVC.maxTime = time
    
        self.navigationController?.pushViewController(secondVC, animated: true)
    }
    
}

