//
//  SecondViewController.swift
//  Timer
//
//  Created by Olzhas Akhmetov on 19.03.2021.
//

import UIKit

class SecondViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    var timer = Timer()
    
    var time : Int = 120
    
    var maxTime: Int = 0
    
    var isTimerRun = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        time = maxTime
        
        label.text = timeToString(intTime: time)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    @objc func timeCount() {
        label.text = timeToString(intTime: time)
        
        if time == 0 {
            label.text = "Time is done"
            timer.invalidate()
            isTimerRun = false
            return
        }
        
        time = time - 1
        
        progressView.progress = 1 - Float(time) / Float(maxTime)
        
        print(time)
    }
    
    @IBAction func play(_ sender: Any) {
        if isTimerRun {
            return
        }
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timeCount), userInfo: nil, repeats: true)
        isTimerRun = true
        progressView.progress = 0
    }
    
    @IBAction func pause(_ sender: Any) {
        timer.invalidate()
        isTimerRun = false
    }
    
    @IBAction func restart(_ sender: Any) {
        timer.invalidate()
        isTimerRun = false
        time = maxTime
        label.text = timeToString(intTime: time)
        progressView.progress = 0
    }
    
    func timeToString(intTime: Int) -> String {
        
        let seconds = intTime % 60
        let minutes = (intTime / 60) % 60
        let hours = (intTime / 3600)
        
        return String(format: "%0.2d:%0.2d:%0.2d", hours, minutes, seconds)
        //"00:00:00"
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
